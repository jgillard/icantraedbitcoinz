'use strict';

////////// LOADS OF SHITTY GLOBALS //////////

const day_ms = 86400000;

// defaultPeriod is time period to display on chart
const defaultPeriod = day_ms;
// granularityPeriods is the number of candlesticks to show
const granularityPeriods = 200;
// earliestEndTimeAgo is how far back data can be drawn from
const earliestEndTimeAgo = 60 * day_ms;

let endGlobal = null;
let granularityGlobal = null;
let bidPrice = null;
let balance = 1000;

let layout = null;
const graphDiv = 'plotly-div';

////////// DOCUMENT SETUP //////////

$(document).ready(function() {
  $('#default-period').html(defaultPeriod);
  $('#granularity-periods').html(granularityPeriods);
  $('#earliest-end-time-ago').html(earliestEndTimeAgo);
  $('#balance').html(balance);

  $('#button').click(function(){
    newGraph();
  });
  $('#buy-btn').click(function(){
    processDeal('buy');
  });
  $('#sell-btn').click(function(){
    processDeal('sell');
  });

  newGraph();
});

////////// MAIN FUNCTIONS //////////

const newGraph = function() {
  reset();

  // Pick an end time before now and after a chosen time period ago
  const now = new Date();
  const earliest = new Date() - earliestEndTimeAgo;
  const end = Math.floor(now - Math.random() * ( now - earliest ));
  const start = end - defaultPeriod;
  const granularity = defaultPeriod / granularityPeriods / 1000;

  endGlobal = end;
  granularityGlobal = granularity;

  getHistoricalData(start, end, granularity, function(response) {
    bidPrice = response.close[0];
    var trace1 = makeTrace(response);
    var data = [trace1];
    Plotly.plot(graphDiv, data, layout);
  });
}

const processDeal = function(buyOrSell) {
  $('#buy-btn')[0].disabled = true;
  $('#sell-btn')[0].disabled = true;
  $('#bid-price').html(twodp(bidPrice));

  var start = endGlobal;
  var end = start + 0.25 * day_ms;

  getHistoricalData(start, end, granularityGlobal, function(response) {
    const nextPrice = response.close[0];
    $('#next-price').html(twodp(nextPrice));
    var trace1 = makeTrace(response);
    var data = [trace1];
    layout.yaxis.showticklabels = true,
    layout.shapes = makeBidMarker(response);
    Plotly.plot(graphDiv, data, layout);
    processOutcome(buyOrSell, nextPrice);
  });
}

const processOutcome = function(buyOrSell, nextPrice) {
  const priceChangeFactor = (nextPrice - bidPrice) / bidPrice;
  const priceChangePercentage = onedp(priceChangeFactor * 100);
  let yourPercentage = buyOrSell == 'buy' ? priceChangePercentage : -priceChangePercentage;
  let newBalance = null;
  if (buyOrSell == 'buy') {
    newBalance = balance + balance * priceChangeFactor;
  } else {
    newBalance = balance - balance * priceChangeFactor;
  }
  $('#your-percentage').html(yourPercentage);
  $('#new-balance').html(twodp(newBalance));
  // persist for next game
  balance = newBalance;
}

const reset = function() {
  $('#bid-price').html('???');
  $('#next-price').html('???');
  $('#balance').html(twodp(balance));
  $('#your-percentage').html('...');
  $('#new-balance').html('...');

  $('#buy-btn')[0].disabled = false;
  $('#sell-btn')[0].disabled = false;

  Plotly.purge(graphDiv);

  layout = madeDefaultLayout();
}

////////// DATA FUNCTIONS //////////

const makeTrace = function(response) {
  return {
    x: response.time,
    close: response.close,
    high: response.high,
    low: response.low,
    open: response.open,
    decreasing: {line: {color: 'red'}},
    increasing: {line: {color: 'green'}}, 
    line: {color: 'rgba(31,119,180,1)'},
    type: 'candlestick', 
    xaxis: 'x',
    yaxis: 'y'
  }
}

const makeBidMarker = function(response) {
  return [{
    type: 'rect',
    xref: 'x',
    yref: 'paper',
    x0: response.time.slice(-1)[0] - granularityGlobal,
    y0: 0,
    x1: response.time.slice(-1)[0] - granularityGlobal + 50,
    y1: 1,
    fillcolor: 'black',
    opacity: 1,
    line: {
        width: 0
    }
  }]
}

const madeDefaultLayout = function() {
  return {
    paper_bgcolor: '#EEEEEE',
    plot_bgcolor: '#FFFFFF',
    margin: {
      r: 50, 
      t: 25, 
      b: 40, 
      l: 50
    }, 
    showlegend: false, 
    hovermode: false, 
    xaxis: {
      visible: false,
      rangeslider: {
        visible: false
      },
    },
    yaxis: {
      showticklabels: false
    }
  }
}

const getHistoricalData = function(start, end, granularity, callback) {
  let url = 'https://api.gdax.com/products/BTC-USD/candles';
  url += '?start=' + new Date(start).toISOString();
  url += '&end=' + new Date(end).toISOString();
  url += '&granularity=' + granularity;

  $.get( url, function( response ) {
    // returns array of [ time, low, high, open, close, volume ]
    // need to unzip for plotly candlestick chart
    const arrays = unzipArrays(response);
    let data = {
      time: arrays[0],
      low: arrays[1],
      high: arrays[2],
      open: arrays[3],
      close: arrays[4],
      volume: arrays[5]
    }
    callback(data);
  });
}

////////// HELPER FUNCTIONS //////////

const unzipArrays = function (arr) {
  var elements = arr.length;
  var len = arr[0].length;
  var final = [];

  for (var i = 0; i < len; i++) {
    var temp = [];
    for (var j = 0; j < elements; j++) {
      temp.push(arr[j][i]);
    }
    final.push(temp);
  }

  return final;
};

const onedp = function(float) {
  return Math.round(float * 10) / 10
}

const twodp = function(float) {
  return Math.round(float * 100) / 100
}
